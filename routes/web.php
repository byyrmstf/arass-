<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=> 'checkuser'],function(){
    Route::get('/home','Home@index');
    Route::get('/add_post','AddPost@index');
    Route::post('/add_postnew','AddPost@add_post');
    Route::post('/post','Post@post_comment');
    Route::get('/post/{number}','Post@index');
    Route::post('/commnet','Comment@reply');
    Route::post('/point','Point@post_point');
    Route::post('/image','ImageUpload@index');
    Route::post('/imageUpload','ImageUpload@upload');

});

Route::get('/logout', 'Login@logout');
Route::post('/login', 'Login@post_login');
Route::get('/login', 'Login@index');


