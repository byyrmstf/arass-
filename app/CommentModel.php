<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    protected $table='comment';
    protected $fillable= ['user_id','comment','post_id', 'sub_id'];
}
