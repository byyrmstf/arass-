<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginModel extends Model
{
    public $timestamps = false ;
    protected $table ='user';
    protected $primarykey= 'id';
    protected $casts = ['id'=>"INT"];
    protected $filltable = ['name','surnama','email','password'];
}
