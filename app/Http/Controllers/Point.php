<?php

namespace App\Http\Controllers;

use App\PointModel;
use Illuminate\Http\Request;

class Point extends Controller
{
    public function post_point(Request $request){

        $data = request()->except(['_token']);

        //print_r($data);
        PointModel::insert($data);
        return redirect('post/'.$data['post_id']);
    }
}
