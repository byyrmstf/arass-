<?php

namespace App\Http\Controllers;


use App\CommentModel;
use Illuminate\Http\Request;

class Comment extends Controller
{

   public function reply(Request $request){
       $data = request()->except(['_token']);

       //print_r($data);

       CommentModel::insert($data);
       return redirect('post/'.$data['post_id']);

   }
}
