<?php

namespace App\Http\Controllers;

use App\PostModel;
use Illuminate\Http\Request;

class Home extends Controller
{
    public function index(){
        $data = array (
            'allPost' => PostModel::get(),
        );



        return view('home',$data);
    }
}
