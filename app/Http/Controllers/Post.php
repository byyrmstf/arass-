<?php

namespace App\Http\Controllers;

use App\LoginModel;
use App\PostModel;
use App\CommentModel;
use App\PointModel;
use App\ImageModel;

use Illuminate\Http\Request;

class Post extends Controller
{
    public function index($id){

        $post = PostModel::where('id',$id)->get();
        $allPoint = PointModel::where('post_id',$id)->get();

        $sum= 0;
        $avg=0.0;
        foreach ($allPoint as $item) {
            $sum+= $item->rank;
        }
        if (count($allPoint)>0) $avg=$sum/count($allPoint);
        //echo $avg;

        $pointData= array (
            'user_id'=>session('id')[0],
            'post_id'=> $id,
        );
        $anyRank = PointModel::where($pointData)->get();


        if(isset($post[0]->user_id)) $user_id = $post[0]->user_id;

        /*
         * USER DAHA ÖNCE OY KULLANDIYSA RANK DEGERİ 0 OLUR
         * KULLANMADIYSA RANK DEGERİ 1 OLUCAK
         */
        if(isset($anyRank[0]->rank))  $rank = 0;
        else $rank = 1 ;


        $data = array (
            'post' => PostModel::where('id',$id)->get(),
            'comment'=> CommentModel::where('post_id',$id)->get(),
            'writer' => LoginModel::where('id', $user_id)->get(),
            'post_id'=> $id,
            'rank'=>$rank,
            'avg'=>$avg,
            'images' => ImageModel::where('post_id',$id)->get(),
            );


        //print_r($data['images']);
        return view('post', $data );
    }

    public function post_comment(Request $request){

        $data = request()->except(['_token']);

        //print_r($data);
        CommentModel::insert($data);
        return redirect('post/'.$data['post_id']);
    }



}
