<?php

namespace App\Http\Controllers;

use \Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\ImageModel;


class ImageUpload extends Controller
{
    public function index(){


        return view('imageUpload');
    }
    public function upload(Request $request){
        $data = request()->except(['_token']);

        //print_r($_FILES);

        $director='uploads/img';
        for ($i = 0 ; $i < count($_FILES['image']['name']); $i++) {
            if(!empty($_FILES['image']['name'][$i])) {
                Storage::disk('uploads')->makeDirectory('img');
                move_uploaded_file($_FILES['image']['tmp_name'][$i],$director."/".$_FILES['image']['size'][$i].$_FILES['image']['name'][$i]);

                $sData = array(
                    'post_id' => $data['post_id'],
                    'url' => $_FILES['image']['size'][$i].$_FILES['image']['name'][$i],
                );
                //print_r($sData);
                ImageModel::insert($sData);
            }
        }
         return redirect('home');
    }
}
