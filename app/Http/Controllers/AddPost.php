<?php

namespace App\Http\Controllers;

use App\PostModel;
use Illuminate\Http\Request;

class AddPost extends Controller
{

    public function index(){

        return view('add_post');
    }

    public function add_post(Request $request){

        $data = request()->except(['_token']);
        PostModel::insert($data);

        $post = PostModel::where($data)->get();
        $post_id = $post[0]->id;

        return view('imageUpload')->with('post_id',$post_id);
    }
}
