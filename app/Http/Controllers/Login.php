<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\LoginModel;
use Illuminate\Http\Request;

class Login extends Controller
{
    public function index()
    {
        return view('/login');
    }

    public function post_login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $data = array (
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        );

        $checkuser = LoginModel::selectRaw("Count(*) as Total")->where($data)->first();
        $user = LoginModel::where($data)->select('*')->first();
        print_r($user['name']);

        if (intval($checkuser->Total) > 0) {
            $getpassword=LoginModel::select("password")->where('email','=',$email)->first();

            if($getpassword->password) {
                $request->session()->push('id', $user['id']);
                $request->session()->push('name', $user['name']);
                $request->session()->push('email', $user['email']);
                $request->session()->push('password', $user['password']);


                return redirect('/home');
            }
            else {
                return view('/login');
            }
        }

        else {
            return view('/login');
        }
    }

    public function logout(Request $request ){

        $request->session()->flush();
        return redirect('/login');

    }
}
