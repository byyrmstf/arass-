<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointModel extends Model
{
    protected $table='point';
    protected $fillable= ['user_id','rank','post_id'];
}
