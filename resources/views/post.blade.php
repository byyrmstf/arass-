<?php  // echo '<pre>'; print_r($images); echo '<pre>'; die();  ?>

@extends('public')
@section('content')

    <section class="content">

        <div class="container-fluid">

            <div class="block-header">
                <h2>
                    POST CONTENT
                </h2>
            </div>
            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <!-- Post Content -->
                    <div class="card">
                        <?php
                        if(isset($post)){
                        foreach ($post as $value){  ?>
                        <div class="header">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h2>
                                    Written by : <?= $writer[0]->email ?>
                                </h2>
                            </div>

                        </div>

                        <div class="body">
                            <h2>
                                <?= $value->title ?>

                            </h2>
                            <p class="m-b-30">
                                <?= $value->content ?>
                            </p>
                            <?php if($rank === 1) {
                            /*
                             * DEGER 1 ISE KULLANICI DAHA ÖNCE KULLANMAMIS DEMEKTIR
                             * BOYLECE PUAMLAMA FORMINI KULLANABİLİR
                             *
                             */
                            ?>
                            <form id="point" action="{{URL::to('/point')}}" method="POST">
                                {{csrf_field()}}
                                <div class="col-sm-2 pull-left">
                                    <!-- Writer Id -->
                                    <input type="hidden" name="user_id" class="form-control"
                                           value="<?= session('id')[0] ?>"/>
                                    <!-- Post Id -->
                                    <input type="hidden" name="post_id" class="form-control"
                                           value="<?= $post_id ?>"/>
                                    <select name="rank" class="form-control show-tick">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>

                                <div class="col-xs-2 pull-left">
                                    <button class="btn btn-block bg-blue waves-effect" type="submit">Point</button>
                                </div>
                            </form>
                            <?php  } ?>

                            <div class="col-sm-2 pull-right">
                                Avarege Points : <?= $avg ?>
                            </div>


                        </div>
                        <div class="body">


                        </div>
                        <?php }
                        } ?>
                    </div>

                </div>
            </div>

            <!-- All Comment -->
            <?php if(isset($comment)) {
            foreach ($comment as $item) {
            if( $item->parent_id === 0  ) { ?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <blockquote class="blockquote-reverse">
                                <p><?= $item->comment ?></p>
                                <footer><cite title="Source Title">Parent Comment </cite></footer>
                            </blockquote>
                        </div>

                        <!-- Sub Comment -->
                        <?php for ($i = 0; $i < count($comment); $i++) {
                        if ($item->id === $comment[$i]->parent_id) { ?>
                        <div class="body">
                            <blockquote class="blockquote-reverse">
                                <p><?= $comment[$i]->comment ?></p>
                                <footer>Reply comment -<cite title="Source Title"><?= $item->user_id ?></cite></footer>
                            </blockquote>
                        </div>
                    <?php }
                    } ?>

                    <!-- Reply Form -->
                        <div class="body">
                            <div class="clearfix">
                                <form id="reply" action="{{URL::to('/commnet')}}"
                                      method="POST">{{csrf_field()}}
                                <!-- Writer Id -->
                                    <input type="hidden" name="user_id" class="form-control"
                                           value="<?= session('id')[0] ?>"/>
                                    <!-- Post Id -->
                                    <input type="hidden" name="post_id" class="form-control"
                                           value="<?= $post_id ?>"/>
                                    <!-- Parent Id -->
                                    <input type="hidden" name="parent_id" class="form-control"
                                           value="<?= $item->id ?>"/>

                                    <!-- isParent-->
                                    <input type="hidden" name="isParent" class="form-control"
                                           value="0"/>

                                    <!-- Commnet -->
                                    <div class="col-lg-10 col-md-10 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control"
                                                       placeholder="Add Comment"
                                                       name="comment" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Submit Button -->
                                    <div class="col-xs-2 pull-right">
                                        <button class="btn btn-block bg-blue waves-effect"
                                                type="submit">SEND
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php } } } ?>

        <!-- New Comment -->
            <div class="card">
                <div class="body">
                    <div class="clearfix">

                        <form id="comment" action="{{URL::to('/post')}}" method="POST">
                        {{csrf_field()}}


                        <!-- Writer Id -->
                            <input type="hidden" name="user_id" class="form-control"
                                   value="<?= session('id')[0] ?>"/>

                            <!-- Post Id -->
                            <input type="hidden" name="post_id" class="form-control"
                                   value="<?= $post_id ?>"/>

                            <!-- isParent-->
                            <input type="hidden" name="isParent" class="form-control"
                                   value="1"/>


                            <!-- Commnet -->
                            <div class="col-lg-10 col-md-10 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Add Comment"
                                               name="comment" required>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xs-2 pull-right">
                                <button class="btn btn-block bg-blue waves-effect" type="submit">SEND
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- Images -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>IMAGES</h2>
                </div>
                <div class="body">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">




                        <!-- Wrapper for slides -->
                        <?php if(isset($images)) {
                        foreach ($images as $key => $image) { ?>

                        <div class="carousel-inner" role="listbox">
                            <div class="item <?= $key !== -1 ? "active" : "" ?>">
                                <img src="/uploads/img/<?= $image->url ?>"/>
                            </div>
                        </div>
                    <?php }
                    }?>

                    <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        </div>
        </div>
    </section>

@endsection
@section('cs')


@endsection
@section('js')


@endsection
