<!DOCTYPE html>
<html>



<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Basic Card | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="/frontend/favicon.ico" type="image/x-icon">
    @yield('css')

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Dropzone Css -->
    <link href="/frontend/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="/frontend/plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="/frontend/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/frontend/plugins/node-waves/waves.css" rel="stylesheet" />

    <!--WaitMe Css-->
    <link href="/frontend/plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/frontend/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/frontend/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/frontend/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="/frontend/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
</head>

<body class="theme-red">
<div class="preloader">
    <div class="spinner-layer pl-red">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>
</div>
<p>Please wait...</p>
</div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="/home"> WEB SİTE </a>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="frontend/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= session('name')[0] ?></div>
                <div class="email"><?= session('email')[0] ?></div>
                <div class="btn-group user-helper-dropdown">
                    <a href="/logout"><i class="material-icons pull-left">input</i>Sign Out</a>

                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="/home">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>

                <li>
                    <a href="/add_post">
                        <i class="material-icons">add</i>
                        <span>Add New Post</span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
</section>

@yield('content')

<!-- Input Mask Plugin Js -->
<script src="/frontend/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- Multi Select Plugin Js -->
<script src="/frontend/plugins/multi-select/js/jquery.multi-select.js"></script>

<!-- Dropzone Plugin Js -->
<script src="/frontend/plugins/dropzone/dropzone.js"></script>

<!-- Ckeditor -->
<script src="/frontend/plugins/ckeditor/ckeditor.js"></script>

<!-- Jquery Core Js -->
<script src="/frontend/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap Core Js -->
<script src="/frontend/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/frontend/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/frontend/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/frontend/plugins/node-waves/waves.js"></script>

<!-- Wait Me Plugin Js -->
<script src="/frontend/plugins/waitme/waitMe.js"></script>

<!-- Jquery Spinner Plugin Js -->
<script src="/frontend/plugins/jquery-spinner/js/jquery.spinner.js"></script>

<!-- noUISlider Plugin Js -->
<script src="/frontend/plugins/nouislider/nouislider.js"></script>





<!-- Autosize Plugin Js -->
<script src="/frontend/plugins/autosize/autosize.js"></script>

<!-- Moment Plugin Js -->
<script src="/frontend/plugins/momentjs/moment.js"></script>


<!-- Custom Js -->
<script src="/frontend/js/admin.js"></script>
<script src="/frontend/js/pages/forms/basic-form-elements.js"></script>

<!-- Demo Js -->
<script src="/frontend/js/demo.js"></script>


<!-- Custom Js -->
<script src="/frontend/js/admin.js"></script>
<script src="/frontend/js/pages/cards/basic.js"></script>
@yield('js')
<!-- Demo Js -->
<script src="/frontend/js/demo.js"></script>
</body>
</html>
