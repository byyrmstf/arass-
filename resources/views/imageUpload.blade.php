<?php // echo '<pre>'; print_r($post_id); echo '<pre>'; die();  ?>



@extends('public')
@section('content')


    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADD POST IMAGE</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <h2 class="card-inside-title"></h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <form id="upload" action="{{URL::to('/imageUpload')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        <input type="hidden" name="post_id" class="form-control" value="<?= $post_id ?>"/>


                                        <div class="form-group">
                                            <div class="form-line">
                                                <input name="image[]" type="file" multiple/>
                                            </div>
                                        </div>


                                        <!-- Submit Botton -->
                                        <div class="col-xs-2 pull-right">
                                            <button class="btn btn-block bg-blue waves-effect" type="submit">SAVE</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('cs')


@endsection
@section('js')


@endsection
