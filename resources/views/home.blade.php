<?php //echo '<pre>'; print_r($allPost); echo '<pre>'; die();  ?>

@extends('public')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ALL POST</h2>
            </div>
        <?php
        if(isset($allPost)){
        foreach ($allPost as $value){  ?>
        <!-- Basic Card -->
            <a href="post/<?= $value->id ?>">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <?= $value->title ?>
                                </h2>
                            </div>
                            <div class="body">
                                <?= $value->content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <!-- #END# Basic Card -->
            <?php }
            } ?>
        </div>
    </section>


@endsection
@section('cs')


@endsection
@section('js')


@endsection
