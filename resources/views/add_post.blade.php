@extends('public')
@section('content')


    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADD NEW POST</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Post Information</h2>
                        </div>
                        <div class="body">
                            <h2 class="card-inside-title"></h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <form id="add_post" action="{{URL::to('/add_postnew')}}" method="post">
                                    {{csrf_field()}}


                                        <input type="hidden" name="user_id" class="form-control" value="<?= session('id')[0] ?>"/>

                                        <!-- Title -->
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="title" class="form-control"
                                                       placeholder="Title" required/>
                                            </div>
                                        </div>

                                        <!-- Content -->
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" class="form-control no-resize" name="content"
                                                          placeholder="Text Area..." required></textarea>
                                            </div>
                                        </div>



                                        <!-- Submit Botton -->
                                        <div class="col-xs-2 pull-right">
                                            <button class="btn btn-block bg-blue waves-effect" type="submit">SAVE</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('cs')


@endsection
@section('js')


@endsection
